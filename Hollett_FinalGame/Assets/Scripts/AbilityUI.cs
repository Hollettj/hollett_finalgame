﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AbilityUI : MonoBehaviour
{
	#region Singleton
	public static AbilityUI instance;

	private void Awake()
	{
		if (instance == null)
		{
			instance = this;
		}
		else
		{
			Destroy(gameObject);
		}
	}
	#endregion

	public Text M_school;
	public Text M_Effect;

	public Image Ability1;
	public Image Ability2;
	public Image Ability3;

	public void ChangeSchoolText(string currentSchool)
	{
		M_school.text = "School: " + currentSchool;
	}

	public void ChangeEffectText(string currentEffect)
	{
		M_Effect.text = "Effect: " + currentEffect;
	}

	public void DecreaseIconOpacity (int icon)
	{
		switch (icon)
		{
			case 1:
				Ability1.color = Ability1.color - new Color(0, 0, 0, .7f);
				break;
			case 2:
				Ability2.color = Ability2.color - new Color(0, 0, 0, .7f);
				break;
			case 3:
				Ability3.color = Ability3.color - new Color(0, 0, 0, .7f);
				break;
		}
	}

	public void IncreaseIconOpacity(int icon)
	{
		switch (icon)
		{
			case 1:
				Ability1.color = Ability1.color + new Color(0, 0, 0, .7f);
				break;
			case 2:
				Ability2.color = Ability2.color + new Color(0, 0, 0, .7f);
				break;
			case 3:
				Ability3.color = Ability3.color + new Color(0, 0, 0, .7f);
				break;
		}
	}
}
