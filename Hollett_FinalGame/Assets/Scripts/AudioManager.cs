﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
	#region Singleton
	public static AudioManager instance;

	private void Awake()
	{
		if (instance == null)
		{
			instance = this;
			// DontDestroyOnLoad(gameObject);
		}
		else
		{
			Destroy(gameObject);
		}
	}
	#endregion

	public AudioSource Ambient;
	public AudioSource Battle;

	private void Start()
	{
		Invoke("PauseMusic", .01f);
	}

	void PauseMusic()
	{
		Battle.Pause();
	}
}
