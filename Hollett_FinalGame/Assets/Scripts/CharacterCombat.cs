﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharacterStats))]
public class CharacterCombat : MonoBehaviour
{
	CharacterStats myStats;

	public float AttackSpeed = 1f;
	private float AttackCoolDown = 0f;
	public float AttackDelay = .6f;

	public event System.Action OnAttack;

	private void Start()
	{
		myStats = GetComponent<CharacterStats>();
	}

	private void Update()
	{
		AttackCoolDown -= Time.deltaTime;
	}

	public void Attack(CharacterStats targetStats)
	{
		if (AttackCoolDown <= 0f)
		{
			StartCoroutine(DoDamage(targetStats, AttackDelay));

			if (OnAttack != null)
			{
				OnAttack();
			}

			AttackCoolDown = 1f / AttackSpeed;
		}
	}

	IEnumerator DoDamage(CharacterStats stats, float delay)
	{
		yield return new WaitForSeconds(delay);

		stats.TakeDamage(myStats.damage.GetValue());
	}
}
