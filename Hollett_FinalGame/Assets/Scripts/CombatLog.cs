﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CombatLog : MonoBehaviour
{
	#region Singleton
	public static CombatLog instance;

	private void Awake()
	{
		if (instance == null)
		{
			instance = this;
			// DontDestroyOnLoad(gameObject);
		}
		else
		{
			Destroy(gameObject);
		}
	}
	#endregion

	public Text[] Log;

	public void AddToLog(string text)
	{
		if (Log[0].text == "")
		{
			Log[0].text = text;
		}
		else if (Log[1].text == "")
		{
			Log[1].text = text;
		}
		else if (Log[2].text == "")
		{
			Log[2].text = text;
		}
		else if (Log[3].text == "")
		{
			Log[3].text = text;
		}
		else if (Log[4].text == "")
		{
			Log[4].text = text;
		}
		else
		{
			MoveTextUp(text);
		}
	}

	void MoveTextUp(string newText)
	{
		Log[4].text = Log[3].text;
		Log[3].text = Log[2].text;
		Log[2].text = Log[1].text;
		Log[1].text = Log[0].text;
		Log[0].text = newText;
	}
}
