﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharacterStats))]
public class Enemy : Interactable
{
	PlayerManager playerManager;
	CharacterStats myStats;
	CharacterCombat playerCombat;

	private void Start()
	{
		playerManager = PlayerManager.instance;
		myStats = GetComponent<CharacterStats>();
		playerCombat = playerManager.player.GetComponent<CharacterCombat>();
	}

	public override void Interact()
	{
		base.Interact();

		if (playerCombat != null)
		{
			playerCombat.Attack(myStats);
		}
	}

	private void OnTriggerStay(Collider other)
	{
		if (other.CompareTag("BladeSweepAoE"))
		{
			if (playerCombat != null)
			{
				playerCombat.Attack(myStats);
			}
		}
	}
}
