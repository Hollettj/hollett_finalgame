﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Audio;

public class EnemyController : MonoBehaviour
{
	public float LookRadius = 10f;

	Transform target;
	NavMeshAgent agent;
	CharacterCombat combat;

	public static int Combatants;
	public static bool CombatIntiated = false;
	public bool InCombat = false;
	public AudioSource Ambient;
	public AudioSource Battle;
	public float fade = 10;

    // Start is called before the first frame update
    void Start()
    {
		agent = GetComponent<NavMeshAgent>();
		target = PlayerManager.instance.player.transform;
		combat = GetComponent<CharacterCombat>();

		Ambient = AudioManager.instance.Ambient;
		Battle = AudioManager.instance.Battle;
    }

    // Update is called once per frame
    void Update()
    {
		float distance = Vector3.Distance(target.position, transform.position);

		if (distance <= LookRadius)
		{
			agent.SetDestination(target.position);

			if (distance <= agent.stoppingDistance)
			{
				CharacterStats targetStats = target.GetComponent<CharacterStats>();
				if (targetStats != null)
				{
					combat.Attack(targetStats);
				}
				FaceTarget();
			}

			if (InCombat == false)
			{
				InCombat = true;
				Combatants += 1;
				CombatIntiated = true;
			}
		}
		else
		{
			if (InCombat == true)
			{
				InCombat = false;
				Combatants -= 1;
			}
		}

		if (Combatants > 0)
		{
			StartCombatMusic();
		}
		else if (Combatants <= 0 && CombatIntiated)
		{
			CombatIntiated = false;
			EndCombatMusic();
		}
    }

	void FaceTarget()
	{
		Vector3 direction = (target.position - transform.position).normalized;
		Quaternion lookrotation = Quaternion.LookRotation(new Vector3(direction.x, 0, direction.z));
		transform.rotation = Quaternion.Slerp(transform.rotation, lookrotation, Time.deltaTime * 5f);
	}

	private void OnDrawGizmosSelected()
	{
		Gizmos.color = Color.red;
		Gizmos.DrawWireSphere(transform.position, LookRadius);
	}

	void StartCombatMusic()
	{
		Battle.UnPause();
		/*for(int i = 0; i >= fade; i++)
		{
			Battle.volume += .01f;
			Ambient.volume -= .01f;
		}*/
		Ambient.Pause();
	}

	void EndCombatMusic()
	{
		Ambient.UnPause();
		/*for (int i = 0; i >= fade; i++)
		{
			Ambient.volume += .01f;
			Battle.volume -= .01f;
		}*/
		Battle.Pause();
	}
}
