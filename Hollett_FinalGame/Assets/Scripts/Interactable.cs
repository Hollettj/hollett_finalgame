﻿using UnityEngine;

public class Interactable : MonoBehaviour
{
	public float radius = 3f;
	public bool isFocused = false;
	bool hasInteracted = false;
	Transform player;
	public Transform interactionTransform;

	private void Update()
	{
		if (isFocused && !hasInteracted)
		{
			float distance = Vector3.Distance(player.position, interactionTransform.position);
			if (distance <= radius)
			{
				Interact();
				hasInteracted = true;
			}
		}
	}

	public virtual void Interact()
	{
		// this method gets overriden for specific interactable
		// print("Interacting with " + transform.name);
	}

	public void OnFocused (Transform playerTransform)
	{
		isFocused = true;
		player = playerTransform;
		hasInteracted = false;
	}

	public void onDefocused()
	{
		isFocused = false;
		player = null;
		hasInteracted = false;
	}

	private void OnDrawGizmosSelected()
	{
		if (interactionTransform == null)
		{
			interactionTransform = transform;
		}

		Gizmos.color = Color.yellow;
		Gizmos.DrawWireSphere(interactionTransform.position, radius);
	}
}
