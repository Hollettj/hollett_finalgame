﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EquipmentManager : MonoBehaviour
{
	#region Singleton
	public static EquipmentManager instance;

	private void Awake()
	{
		if (instance == null)
		{
			instance = this;
			// DontDestroyOnLoad(gameObject);
		}
		else
		{
			Destroy(gameObject);
		}
	}
	#endregion

	Equipment[] currentEquipment;

	Inventory inventory;

	public EquipmentSlot[] slots;

	public delegate void OnEquipmentChanged(Equipment newItem, Equipment oldItem);
	public OnEquipmentChanged onEquipmentChangedCallback;

	private void Start()
	{
		inventory = Inventory.instance;

		int numSlots = System.Enum.GetNames(typeof(EquipmentSlots)).Length;
		currentEquipment = new Equipment[numSlots];
	}

	public void Equip(Equipment newItem)
	{
		int slotIndex = (int)newItem.equipSlot;

		Equipment oldItem = null;
		if (currentEquipment[slotIndex] != null)
		{
			oldItem = currentEquipment[slotIndex];
			inventory.Add(oldItem);
		}

		if (onEquipmentChangedCallback != null)
		{
			onEquipmentChangedCallback.Invoke(newItem, oldItem);
		}
		currentEquipment[slotIndex] = newItem;

		slots[slotIndex].AddItem(newItem);
	}

	public void Unequip (int slotIndex)
	{
		if (currentEquipment[slotIndex] != null)
		{
			Equipment oldItem = currentEquipment[slotIndex];
			inventory.Add(oldItem);

			currentEquipment[slotIndex] = null;
			slots[slotIndex].ClearSlot();

			if (onEquipmentChangedCallback != null)
			{
				onEquipmentChangedCallback.Invoke(null, oldItem);
			}
		}
	}

	public void UnequipAll()
	{
		for(int i = 0; i < currentEquipment.Length; i++)
		{
			Unequip(i);
		}
	}

	private void Update()
	{
		if (Input.GetKeyDown(KeyCode.U))
		{
			UnequipAll();
		}
	}
}
