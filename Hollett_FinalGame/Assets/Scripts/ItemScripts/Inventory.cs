﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour
{
	public List<Item> items = new List<Item>();

	#region Singleton
	public static Inventory instance;

	private void Awake()
	{
		if (instance == null)
		{
			instance = this;
			// DontDestroyOnLoad(gameObject);
		}
		else
		{
			Destroy(gameObject);
		}
	}
	#endregion

	public delegate void OnItemChanged();
	public OnItemChanged onItemChangedCallBack;

	public int Space = 20;

	public bool Add (Item item)
	{
		if (!item.isDefaultItem)
		{
			if (items.Count >= Space)
			{
				print("Not Enough Space");
				return false;
			}
			
			items.Add(item);

			if (onItemChangedCallBack != null)
			{
				onItemChangedCallBack.Invoke();
			}
		}
		return true;
	}

	public void Remove(Item item)
	{
		items.Remove(item);
		onItemChangedCallBack.Invoke();
	}
}
