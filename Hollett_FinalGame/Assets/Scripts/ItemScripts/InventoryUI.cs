﻿using UnityEngine;

public class InventoryUI : MonoBehaviour
{
	Inventory inventory;

	public Transform I_panel;
	InventorySlot[] slots;

	public GameObject inventoryUI;

    // Start is called before the first frame update
    void Start()
    {
		inventory = Inventory.instance;
		inventory.onItemChangedCallBack += UpdateUI;

		slots = I_panel.GetComponentsInChildren<InventorySlot>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Inventory"))
		{
			inventoryUI.SetActive(!inventoryUI.activeSelf);
		}
    }

	void UpdateUI()
	{
		print("Updating UI");

		for (int i = 0; i < slots.Length; i++)
		{
			if (i < inventory.items.Count)
			{
				slots[i].AddItem(inventory.items[i]);
			}
			else
			{
				slots[i].ClearSlot();
			}
		}
	}
}
