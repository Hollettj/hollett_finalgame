﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ItemButton : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
	[SerializeField] private TooltipPopUp tooltipPopUp;
	[SerializeField] private InventorySlot slot;

   public void OnPointerEnter (PointerEventData eventData)
	{
		// tooltipPopUp.DisplayInfo(slot.item);
	}

	public void OnPointerExit(PointerEventData eventData)
	{
		tooltipPopUp.HideInfo();
	}
}
