﻿using UnityEngine;

public class ItemPickUp : Interactable
{
	public Item item;
	
	public override void Interact()
	{
		base.Interact();

		PickUp();
	}

	void PickUp()
	{
		print("Picking up an " + item.name);
		bool wasPickedUp = Inventory.instance.Add(item);
		if (wasPickedUp)
		{
			Destroy(gameObject);
		}
	}
}
