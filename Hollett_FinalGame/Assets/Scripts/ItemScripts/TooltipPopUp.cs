﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TooltipPopUp : MonoBehaviour
{
	[SerializeField] private GameObject popupCanvasObj;
	[SerializeField] private RectTransform popupObj;
	[SerializeField] private Vector3 offset;
	[SerializeField] private float padding;

	private Canvas popupCanvas;

	public Text nameText;
	public Text attackText;
	public Text defenseText;

	private void Awake()
	{
		popupCanvas = popupCanvasObj.GetComponent<Canvas>();
	}

	private void Update()
	{
		FollowCursor();
	}

	private void FollowCursor()
	{
		if (!popupCanvasObj.activeSelf) { return; }

		Vector3 newPos = Input.mousePosition + offset;
		newPos.z = 0f;

		float rightEdgeToScreenEdgeDistance = Screen.width - (newPos.x + popupObj.rect.width * popupCanvas.scaleFactor / 2) + padding;
		if (rightEdgeToScreenEdgeDistance < 0)
		{
			newPos.x += rightEdgeToScreenEdgeDistance;
		}
		float leftEdgeToScreenEdgeDistance = 0 - (newPos.x - popupObj.rect.width * popupCanvas.scaleFactor / 2) + padding;
		if (leftEdgeToScreenEdgeDistance < 0)
		{
			newPos.x += leftEdgeToScreenEdgeDistance;
		}
		float topEdgeToScreenEdgeDistance = Screen.height - (newPos.y + popupObj.rect.height * popupCanvas.scaleFactor) - padding;
		if (topEdgeToScreenEdgeDistance < 0)
		{
			newPos.y += topEdgeToScreenEdgeDistance;
		}
		popupObj.transform.position = newPos;
	}

	public void DisplayInfo (Equipment equipment)
	{
		nameText.text = equipment.name;
		attackText.text = "Attack: " + equipment.damageModifier;
		defenseText.text = "Defense: " + equipment.armourModifier;

		popupCanvasObj.SetActive(true);
		LayoutRebuilder.ForceRebuildLayoutImmediate(popupObj);
	}

	public void HideInfo ()
	{
		popupCanvasObj.SetActive(false);
	}
}