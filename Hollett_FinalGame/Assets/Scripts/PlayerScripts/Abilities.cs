﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Abilities : MonoBehaviour
{
	float spellCooldown = 0f;
	public float spellCDRate = 4f;
	bool spellOpacityDecreased = false;

	public School currentSchool = School.Fire;
	public Effect currentEffect = Effect.Projectile;
	public Spell currentSpell = Spell.Fireball;

	PlayerController p_Controller;
	Transform player;

	public int Fireballdmg = 20;
	public int BurningHandsdmg = 30;
	public int Frostbitedmg = 10;
	public float FrostbiteDuration = 5f;
	public float FrostbiteIntensity = .5f;
	public int ChillTouchdmg = 15;
	public float ChillTouchDuration = 8f;
	public float ChillTouchIntensity = .75f;

	PlayerMotor motor;

    // Start is called before the first frame update
    void Start()
    {
		p_Controller = GetComponent<PlayerController>();
		player = PlayerManager.instance.player.transform;
		motor = GetComponent<PlayerMotor>();
    }

    // Update is called once per frame
    void Update()
    {
		spellCooldown -= Time.deltaTime;

		if (spellCooldown <= 0 && spellOpacityDecreased)
		{
			AbilityUI.instance.IncreaseIconOpacity(3);
		}
		

        if (Input.GetKeyDown(KeyCode.M)) // changes currently selected school of magic
		{
			switch (currentSchool)
			{
				case School.Fire:
					currentSchool = School.Ice;
					AbilityUI.instance.ChangeSchoolText("Ice");
					break;
				case School.Ice:
					currentSchool = School.Fire;
					AbilityUI.instance.ChangeSchoolText("Fire");
					break;
			}

			ChangeCurrentSpell();
		}

		if (Input.GetKeyDown(KeyCode.E)) // changes currently selected spell effect
		{
			switch (currentEffect)
			{
				case Effect.Projectile:
					currentEffect = Effect.Touch;
					AbilityUI.instance.ChangeEffectText("Touch");
					break;
				case Effect.Touch:
					currentEffect = Effect.Projectile;
					AbilityUI.instance.ChangeEffectText("Projectile");
					break;
			}

			ChangeCurrentSpell();
		}

		if (Input.GetKeyDown(KeyCode.Alpha3) && spellCooldown <= 0f) // casts currently selected spell
		{
			if (p_Controller.focus != null)
			{
				if (spellCooldown <= 0f && p_Controller.focus.CompareTag("Enemy"))
				{
					switch (currentSpell)
					{
						case Spell.Fireball:
							Fireball();
							break;
						case Spell.BurningHands:
							BurningHands();
							break;
						case Spell.Frostbite:
							Frostbite();
							break;
						case Spell.ChillTouch:
							ChillTouch();
							break;
					}

					spellCooldown = spellCDRate;
					AbilityUI.instance.DecreaseIconOpacity(3);
					spellOpacityDecreased = true;
				}
			}
		}
    }

	void ChangeCurrentSpell() // sets the current spell according to what School and Effect are selected
	{
		if (currentSchool == School.Fire && currentEffect == Effect.Projectile)
		{
			currentSpell = Spell.Fireball;
		}
		else if (currentSchool == School.Ice && currentEffect == Effect.Projectile)
		{
			currentSpell = Spell.Frostbite;
		}
		else if (currentSchool == School.Fire && currentEffect == Effect.Touch)
		{
			currentSpell = Spell.BurningHands;
		}
		else if (currentSchool == School.Ice && currentEffect == Effect.Touch)
		{
			currentSpell = Spell.ChillTouch;
		}
	}

	void Fireball()
	{
		print("Casting Fireball");

		Vector3 origin = player.position;
		origin.y = origin.y + .5f;
		Vector3 dir = (p_Controller.focus.transform.position - origin).normalized;
		RaycastHit hit;

		Physics.Raycast(origin, dir, out hit, 30f);
		if (hit.collider.CompareTag("Enemy"))
		{
			CharacterStats stats = hit.collider.GetComponent<CharacterStats>();
			if (stats != null)
			{
				stats.TakeDamage(Fireballdmg);
			}
		}
	}

	void BurningHands()
	{
		print("Casting Burning Hands");

		float distance = Mathf.Abs(Vector3.Distance(motor.target.position, player.position));

		if (distance >= motor.agent.stoppingDistance)
		{
			CharacterStats stats = p_Controller.focus.GetComponent<CharacterStats>();
			stats.TakeDamage(BurningHandsdmg);
		}
	}

	void Frostbite()
	{
		print("Casting Frostbite");

		Vector3 origin = player.position;
		origin.y = origin.y + .5f;
		Vector3 dir = (p_Controller.focus.transform.position - origin).normalized;
		RaycastHit hit;

		Physics.Raycast(origin, dir, out hit, 30f);
		if (hit.collider.CompareTag("Enemy"))
		{
			CharacterStats stats = hit.collider.GetComponent<CharacterStats>();
			CharacterCombat combat = hit.collider.GetComponent<CharacterCombat>();
			if (stats != null)
			{
				stats.TakeDamage(Frostbitedmg);
				if (combat != null)
				{
					StartCoroutine(SlowEffect(combat, FrostbiteDuration, FrostbiteIntensity));
				}
			}
		}
	}

	IEnumerator SlowEffect (CharacterCombat combat, float duration, float slowIntensity)
	{
		combat.AttackSpeed -= slowIntensity; // applies slow effect to target based on provided intensity

		yield return new WaitForSeconds(duration); // waits for provided duration in seconds

		combat.AttackSpeed += slowIntensity; // removes slow effect from target
	}

	void ChillTouch()
	{
		print("Casting Chill Touch");

		float distance = Mathf.Abs(Vector3.Distance(motor.target.position, player.position));

		if (distance >= motor.agent.stoppingDistance)
		{
			CharacterStats stats = p_Controller.focus.GetComponent<CharacterStats>();
			CharacterCombat combat = p_Controller.focus.GetComponent<CharacterCombat>();
			stats.TakeDamage(ChillTouchdmg);
			StartCoroutine(SlowEffect(combat, ChillTouchDuration, ChillTouchIntensity));
		}
	}

	public enum School { Fire, Ice}
	public enum Effect { Projectile, Touch}
	public enum Spell { Fireball, BurningHands, Frostbite, ChillTouch}
}
