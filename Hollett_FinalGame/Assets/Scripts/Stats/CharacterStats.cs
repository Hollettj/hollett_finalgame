﻿using UnityEngine;

public class CharacterStats : MonoBehaviour
{
	public int maxHealth = 100;
	public int currentHealth { get; private set; }

	public Stat damage;
	public Stat armour;

	private void Awake()
	{
		currentHealth = maxHealth;
	}

	private void Update()
	{
		if (Input.GetKeyDown(KeyCode.T)) // test for taking damage
		{
			TakeDamage(10);
		}
	}

	public void TakeDamage(int Damage)
	{
		Damage -= armour.GetValue();
		Damage = Mathf.Clamp(Damage, 0, int.MaxValue);

		currentHealth -= Damage;
		print(transform.name + " takes " + Damage + " damage.");
		string text = transform.name + " takes " + Damage + " damage.";
		CombatLog.instance.AddToLog(text);

		if (currentHealth <= 0)
		{
			Die();
		}
	}

	public virtual void Die()
	{
		// character dies in certain way
		// method meant to be overridden
		print(transform.name + " died.");
		string text = transform.name + " died.";
		CombatLog.instance.AddToLog(text);
	}
}
