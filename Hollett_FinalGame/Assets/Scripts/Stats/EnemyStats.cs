﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyStats : CharacterStats
{
	PlayerController p_controller;

	private void Start()
	{
		p_controller = PlayerManager.instance.player.GetComponent<PlayerController>();
	}

	public override void Die()
	{
		base.Die();

		// add ragdoll effect or death animation

		if (p_controller.focus == this)
		{
			p_controller.RemoveFocus();
		}
		EnemyController.Combatants -= 1;
		Destroy(gameObject);
	}
}
