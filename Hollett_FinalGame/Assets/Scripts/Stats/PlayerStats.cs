﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerStats : CharacterStats
{
	public Stances stance = 0;
	public int dmgIn = 3;
	public float speedIn = .25f;
	public int parryIn = 3;
	CharacterCombat combat;

	PlayerController p_Controller;

	// Text boxes for player info
	public Text AttackText;
	public Text DefenseText;

	public float csDelay = .8f; // crippling strike variables
	public float csCooldown = 0f;
	public float csCooldownRate = 3f;
	bool csOpacityDecreased = false;

	public float bsDelay = .4f; // blade sweep variables
	public float bsCooldown = 0f;
	public float bsCooldownRate = 2f;
	public GameObject bsAoE;
	bool bsOpacityDecreased = false;

    // Start is called before the first frame update
    void Start()
    {
		EquipmentManager.instance.onEquipmentChangedCallback += OnEquipmentChanged;
		combat = GetComponent<CharacterCombat>();
		damage.AddModifier(dmgIn); // adds power stance bonus at beginning, since it's the default stance

		p_Controller = GetComponent<PlayerController>();
    }

    void OnEquipmentChanged(Equipment newItem, Equipment oldItem)
	{
		if (newItem != null)
		{
			armour.AddModifier(newItem.armourModifier);
			damage.AddModifier(newItem.damageModifier);
		}

		if (oldItem != null)
		{
			armour.RemovceModifier(oldItem.armourModifier);
			damage.RemovceModifier(oldItem.damageModifier);
		}
	}

	public override void Die()
	{
		base.Die();
		PlayerManager.instance.KillPlayer();
	}

	// Stance Script
	private void Update()
	{
		#region Stances
		if (Input.GetKeyDown(KeyCode.C))
		{
			string text;

			if (stance == Stances.Power) // changes the current stance into the next stance
			{
				stance = Stances.Speed;
			}
			else if(stance == Stances.Speed)
			{
				stance = Stances.Defense;
			}
			else if(stance == Stances.Defense)
			{
				stance = Stances.Power;
			}

			switch (stance) // applies stance
			{
				case Stances.Power:
					damage.AddModifier(dmgIn); // power stance
					armour.RemovceModifier(parryIn); // removes bonuses of Defense stance
					print("Using Power Stance");
					text = "Using Power Stance.";
					CombatLog.instance.AddToLog(text);
					break;
				case Stances.Speed:
					combat.AttackSpeed += speedIn; // speed stance
					damage.RemovceModifier(dmgIn); // removes bonuses of Power stance
					print("Using Speed Stance");
					text = "Using Speed Stance.";
					CombatLog.instance.AddToLog(text);
					break;
				case Stances.Defense:
					armour.AddModifier(parryIn); // defense stance
					combat.AttackSpeed -= speedIn; // removes bonuses of Speed stance
					print("Using Defense Stance");
					text = "Using Defense Stance.";
					CombatLog.instance.AddToLog(text);
					break;
			}
		}
		#endregion

		csCooldown -= Time.deltaTime;
		bsCooldown -= Time.deltaTime;

		if (csCooldown <= 0 && csOpacityDecreased)
		{
			AbilityUI.instance.IncreaseIconOpacity(1);
		}

		if (bsCooldown <= 0 && bsOpacityDecreased)
		{
			AbilityUI.instance.IncreaseIconOpacity(2);
		}

		if (Input.GetKeyDown(KeyCode.Alpha1))
		{
			if (p_Controller.focus != null)
			{ 
				if (p_Controller.focus.CompareTag("Enemy"))
				{
					CripplingStrike();
				}
			}
		}

		if (Input.GetKeyDown(KeyCode.Alpha2))
		{
			if (p_Controller.focus != null)
			{
				if (p_Controller.focus.CompareTag("Enemy"))
				{
					BladeSweep();
				}
			}
		}

		// Script to update info in inventory panel
		AttackText.text = "Attack: " + damage.GetValue();
		DefenseText.text = "Defense: " + armour.GetValue();
	}

	#region Crippling Strike
	void CripplingStrike()
	{
		CharacterStats stats = p_Controller.focus.GetComponent<CharacterStats>();
		if (stats != null && bsCooldown <= 0)
		{
			StartCoroutine(DoCritdmg(stats, csDelay));
			csCooldown = csCooldownRate;

			AbilityUI.instance.DecreaseIconOpacity(1);
			csOpacityDecreased = true;

			print("Using Crippling Stike");
		}
	}

	IEnumerator DoCritdmg (CharacterStats targetStats, float delay)
	{
		yield return new WaitForSeconds(delay);

		targetStats.TakeDamage(damage.GetValue() * 2);
	}
	#endregion

	#region Blade Sweep
	void BladeSweep ()
	{
		if (bsCooldown <= 0)
		{
			StartCoroutine(Sweep(bsCooldown));
			bsCooldown = bsCooldownRate;

			AbilityUI.instance.DecreaseIconOpacity(2);
			bsOpacityDecreased = true;

			print("Using Blade Sweep");
		}
	}

	IEnumerator Sweep (float delay)
	{
		yield return new WaitForSeconds(delay);

		bsAoE.SetActive(true);

		yield return new WaitForEndOfFrame();

		bsAoE.SetActive(false);
	}
	#endregion
}

public enum Stances : int { Power, Speed, Defense}
