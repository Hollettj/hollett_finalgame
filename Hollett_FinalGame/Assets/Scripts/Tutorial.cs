﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Tutorial : MonoBehaviour
{
	public Text title;
	public Text text;

	public Button button;

	int page;

	private void Start()
	{
		title.text = "Movement";
		text.text = "The player's movement is controlled by the mouse. Left click moves the player; Right click interacts with the object. The 'A' and 'D' keys, and the arrow keys, can be used to rotate the camera.";
	}

	public void OnButtonPress()
	{
		page += 1;
		switch (page)
		{
			case 1:
				title.text = "Inventory";
				text.text = "You can press the 'I' key to open your inventory. Left click an item to equip it. To unequip all items, press the 'U' key. The red 'X' button destroys the item.";
				break;
			case 2:
				title.text = "Combat";
				text.text = "Right click is used to attack enemies. You can also press 'C' to change your stance, which gives a passive bonus. There are three stances; power, speed, and defense.";
				break;
			case 3:
				title.text = "Combat";
				text.text = "You have two abilities, Crippling Strike and Blade Sweep. They're the '1' and '2' key respectively. Crippling Strike hits with powerful strike, dealing extra damage. Blade Sweep slashes in a circle, damaging all in an area around the player.";
				break;
			case 4:
				title.text = "Magic";
				text.text = "Spells are controlled by two factors, school and effect. The school is what type of magic is used, and it can be changed with the 'M' key. The effect is how the magic is used, and it can be changed with the 'E' key. The '3' key casts the spell.";
				break;
			case 5:
				Destroy(gameObject);
				break;
		}
	}
}
